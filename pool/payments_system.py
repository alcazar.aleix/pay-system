import asyncio
import logging
import pathlib
import time
import traceback
from asyncio import Task
from math import floor
from typing import Dict, Optional, Set, List, Tuple, Callable

from datetime import datetime
import hashlib
from pathlib import Path
import sqlite3
import asyncpg
from datetime import date
import time
import math
from datetime import datetime

from blspy import AugSchemeMPL, G1Element
from chia.consensus.block_rewards import calculate_pool_reward
from chia.pools.pool_wallet_info import PoolState, PoolSingletonState
from chia.protocols.pool_protocol import (
    PoolErrorCode,
    PostPartialRequest,
    PostPartialResponse,
    PostFarmerRequest,
    PostFarmerResponse,
    PutFarmerRequest,
    PutFarmerResponse,
    POOL_PROTOCOL_VERSION,
)
from chia.rpc.wallet_rpc_client import WalletRpcClient
from chia.types.blockchain_format.coin import Coin
from chia.types.coin_record import CoinRecord
from chia.types.coin_spend import CoinSpend
from chia.util.bech32m import decode_puzzle_hash
from chia.consensus.constants import ConsensusConstants
from chia.util.ints import uint8, uint16, uint32, uint64
from chia.util.byte_types import hexstr_to_bytes
from chia.util.default_root import DEFAULT_ROOT_PATH
from chia.rpc.full_node_rpc_client import FullNodeRpcClient
from chia.full_node.signage_point import SignagePoint
from chia.types.end_of_slot_bundle import EndOfSubSlotBundle
from chia.types.blockchain_format.sized_bytes import bytes32
from chia.consensus.pot_iterations import calculate_iterations_quality
from chia.util.lru_cache import LRUCache
from chia.util.chia_logging import initialize_logging
from chia.wallet.transaction_record import TransactionRecord
from chia.pools.pool_puzzles import (
    get_most_recent_singleton_coin_from_coin_spend,
    get_delayed_puz_info_from_launcher_spend,
    launcher_id_to_p2_puzzle_hash,
)

from .difficulty_adjustment import get_new_difficulty
from .singleton import create_absorb_transaction, get_singleton_state, get_coin_spend, get_farmed_height
from .store.abstract import AbstractPoolStore
from .store.sqlite_store import SqlitePoolStore
from .record import FarmerRecord
from .util import error_dict, RequestMetadata

# PARA TEST BORRAR EN PRODUCCION
from pprint import pprint


class PaymetsSystem:
    def __init__(
        self,
        config: Dict,
        pool_config: Dict,
        constants: ConsensusConstants,
        pool_store: Optional[AbstractPoolStore] = None,
        difficulty_function: Callable = get_new_difficulty,
    ):
        self.config = config
        self.constants = constants
        self.pool_config = pool_config

        self.follow_singleton_tasks: Dict[bytes32, asyncio.Task] = {}
        self.log = logging
        # If you want to log to a file: use filename='example.log', encoding='utf-8'
        self.log.basicConfig(level=logging.INFO)

        initialize_logging("pool", pool_config["logging"], pathlib.Path(pool_config["logging"]["log_path"]))

        config_path_db = self.pool_config["database"]["path"]

        self.log.info(f"Configurando base de datos: { config_path_db }")

        if (config_path_db) :
            self.db_path = Path(config_path_db)
        else:
            self.db_path = Path("pooldb.sqlite")

        self.connection: Optional[sqlite3.Connection] = None
        self.connection_payments: Optional[asyncpg.Connection] = None

        # Set our pool info here
        self.info_default_res = pool_config["pool_info"]["default_res"]
        self.info_name = pool_config["pool_info"]["name"]
        self.info_logo_url = pool_config["pool_info"]["logo_url"]
        self.info_description = pool_config["pool_info"]["description"]
        self.welcome_message = pool_config["welcome_message"]

        self.store: AbstractPoolStore = pool_store or SqlitePoolStore()

        self.pool_fee = pool_config["pool_fee"]

        # This number should be held constant and be consistent for every pool in the network. DO NOT CHANGE
        self.iters_limit = self.constants.POOL_SUB_SLOT_ITERS // 64

        # This number should not be changed, since users will put this into their singletons
        self.relative_lock_height = uint32(pool_config["relative_lock_height"])

        # TODO(pool): potentially tweak these numbers for security and performance
        # This is what the user enters into the input field. This exact value will be stored on the blockchain
        self.pool_url = pool_config["pool_url"]
        self.min_difficulty = uint64(pool_config["min_difficulty"])  # 10 difficulty is about 1 proof a day per plot
        self.default_difficulty: uint64 = uint64(pool_config["default_difficulty"])
        self.difficulty_function: Callable = difficulty_function

        self.pending_point_partials: Optional[asyncio.Queue] = None
        self.recent_points_added: LRUCache = LRUCache(20000)

        # The time in minutes for an authentication token to be valid. See "Farmer authentication" in SPECIFICATION.md
        self.authentication_token_timeout: uint8 = pool_config["authentication_token_timeout"]

        # This is where the block rewards will get paid out to. The pool needs to support this address forever,
        # since the farmers will encode it into their singleton on the blockchain. WARNING: the default pool code
        # completely spends this wallet and distributes it to users, do don't put any additional funds in here
        # that you do not want to distribute. Even if the funds are in a different address than this one, they WILL
        # be spent by this code! So only put funds that you want to distribute to pool members here.

        # Using 2164248527
        self.default_target_puzzle_hash: bytes32 = bytes32(decode_puzzle_hash(pool_config["default_target_address"]))

        # The pool fees will be sent to this address. This MUST be on a different key than the target_puzzle_hash,
        # otherwise, the fees will be sent to the users. Using 690783650
        self.pool_fee_puzzle_hash: bytes32 = bytes32(decode_puzzle_hash(pool_config["pool_fee_address"]))

        # This is the wallet fingerprint and ID for the wallet spending the funds from `self.default_target_puzzle_hash`
        self.wallet_fingerprint = pool_config["wallet_fingerprint"]
        self.wallet_id = pool_config["wallet_id"]

        # We need to check for slow farmers. If farmers cannot submit proofs in time, they won't be able to win
        # any rewards either. This number can be tweaked to be more or less strict. More strict ensures everyone
        # gets high rewards, but it might cause some of the slower farmers to not be able to participate in the pool.
        self.partial_time_limit: int = pool_config["partial_time_limit"]

        # There is always a risk of a reorg, in which case we cannot reward farmers that submitted partials in that
        # reorg. That is why we have a time delay before changing any account points.
        self.partial_confirmation_delay: int = pool_config["partial_confirmation_delay"]

        # Only allow PUT /farmer per launcher_id every n seconds to prevent difficulty change attacks.
        self.farmer_update_blocked: set = set()
        self.farmer_update_cooldown_seconds: int = 600

        # These are the phs that we want to look for on chain, that we can claim to our pool
        self.scan_p2_singleton_puzzle_hashes: Set[bytes32] = set()

        # Don't scan anything before this height, for efficiency (for example pool start date)
        self.scan_start_height: uint32 = uint32(pool_config["scan_start_height"])

        # Interval for scanning and collecting the pool rewards
        self.collect_pool_rewards_interval = pool_config["collect_pool_rewards_interval"]

        # After this many confirmations, a transaction is considered final and irreversible
        self.confirmation_security_threshold = pool_config["confirmation_security_threshold"]

        # Interval for making payout transactions to farmers
        self.payment_interval = pool_config["payment_interval"]

        # We will not make transactions with more targets than this, to ensure our transaction gets into the blockchain
        # faster.
        self.max_additions_per_transaction = pool_config["max_additions_per_transaction"]

        # This is the list of payments that we have not sent yet, to farmers
        self.pending_payments: Optional[asyncio.Queue] = None

        # Keeps track of the latest state of our node
        self.blockchain_state = {"peak": None}

        # Whether or not the wallet is synced (required to make payments)
        self.wallet_synced = False

        # We target these many partials for this number of seconds. We adjust after receiving this many partials.
        self.number_of_partials_target: int = pool_config["number_of_partials_target"]
        self.time_target: int = pool_config["time_target"]

        # Tasks (infinite While loops) for different purposes
        self.submit_payment_loop_task: Optional[asyncio.Task] = None

        self.node_rpc_client: Optional[FullNodeRpcClient] = None
        self.node_rpc_port = pool_config["node_rpc_port"]
        self.wallet_rpc_client: Optional[WalletRpcClient] = None
        self.wallet_rpc_port = pool_config["wallet_rpc_port"]

    async def start(self):
        await self.store.connect()
        self.pending_point_partials = asyncio.Queue()

        self_hostname = self.config["self_hostname"]
        self.node_rpc_client = await FullNodeRpcClient.create(
            self_hostname, uint16(self.node_rpc_port), DEFAULT_ROOT_PATH, self.config
        )
        self.wallet_rpc_client = await WalletRpcClient.create(
            self.config["self_hostname"], uint16(self.wallet_rpc_port), DEFAULT_ROOT_PATH, self.config
        )
        self.blockchain_state = await self.node_rpc_client.get_blockchain_state()
        res = await self.wallet_rpc_client.log_in_and_skip(fingerprint=self.wallet_fingerprint)
        if not res["success"]:
            raise ValueError(f"Error logging in: {res['error']}. Make sure your config fingerprint is correct.")
        self.log.info(f"Logging in: {res}")
        res = await self.wallet_rpc_client.get_wallet_balance(self.wallet_id)
        self.log.info(f"Obtaining balance: {res}")

        self.scan_p2_singleton_puzzle_hashes = await self.store.get_pay_to_singleton_phs()
        self.submit_payment_loop_task = asyncio.create_task(self.submit_payment_loop())

        self.pending_payments = asyncio.Queue()

    async def stop(self):
        if self.submit_payment_loop_task is not None:
            self.submit_payment_loop_task.cancel()

        self.wallet_rpc_client.close()
        await self.wallet_rpc_client.await_closed()
        self.node_rpc_client.close()
        await self.node_rpc_client.await_closed()
        await self.store.connection.close()



    async def connect_payments_system(self):
        host=self.pool_config["payouts"]["database"]["host"]
        port=self.pool_config["payouts"]["database"]["port"]
        user=self.pool_config["payouts"]["database"]["user"]
        password=self.pool_config["payouts"]["database"]["password"]
        db=self.pool_config["payouts"]["database"]["db"]

        print(f" PUERTO {port}  ")

        print(f" {host}  {port}   {user}   {password}   {db}  ")
        self.connection_payments = await asyncpg.connect(host=host,port=port,user=user,password=password,database=db)
        return self.connection_payments


    async def create_closure(self):
        hash=""
        try:
            total_amount_claimed = await self.total_amount_claimed()

            self.log.info(f"Monto acumulado: {total_amount_claimed}")

            self.connection = sqlite3.connect(self.db_path)

            segundos, timestamp = math.modf(datetime.timestamp(datetime.utcnow()))
            hash = hashlib.md5()
            hash.update(str(timestamp).encode())
            hash = hash.hexdigest()

            cursor = self.connection.cursor()
            self.log.info(f"Agrego closures")
            cursor.execute(
                f"INSERT INTO closures (hash, \"timestamp\", acomulated_amount) VALUES(?, ?, ?)",
                (hash ,timestamp,total_amount_claimed),
            )
            self.log.info(f"Agrego los puntos de los farmers")
            cursor.execute(
                f"INSERT INTO farmers_ponts_of_closure (launcher_id, points, closure_hash ) SELECT launcher_id, points ,'{hash}' "\
                f"FROM farmer WHERE is_pool_member = 1"
            )
            self.log.info(f"Actualizo puntos")
            cursor.execute( f"UPDATE farmer SET points=0")
            self.connection.commit()
            print("Record inserted successfully into SqliteDb_developers table ", cursor.rowcount)
            cursor.close()

        except sqlite3.Error as error:
            print("Failed to insert data into sqlite table", error)
        finally:
            if self.connection:
                self.connection.close()
                print("The SQLite connection is closed")
        return hash


    async def total_amount_claimed(self):
        total_coins = 0
        is_sync = False
        while not is_sync:
            try:
                if self.blockchain_state["sync"]["synced"]:
                    is_sync = True
                else:
                    self.log.warning("Not synced, waiting")
                    await asyncio.sleep(60)
            except asyncio.CancelledError:
                self.log.info("Cancelled create_payments_loop, closing")
                return
            except Exception as e:
                error_stack = traceback.format_exc()
                self.log.error(f"Unexpected error in create_payments_loop: {e} {error_stack}")
                await asyncio.sleep(self.payment_interval)

        coin_records: List[CoinRecord] = await self.node_rpc_client.get_coin_records_by_puzzle_hash(
            self.default_target_puzzle_hash,
            include_spent_coins=False,
            start_height=self.scan_start_height,
        )

        if len(coin_records) != 0:
            total_coins = ( sum([c.coin.amount for c in coin_records]) ) / (10**12) 
        
        return total_coins



    async def farmer_update_state(self,launcher_id,new_state):
        try:
            self.connection_payments = await self.connect_payments_system()
            await self.connection_payments.execute('''INSERT INTO public.current_states_of_farmers 
                                                            (datetime, states_of_farmers_id, farmers_id)
                                                        values ( CURRENT_TIMESTAMP,
                                                            (SELECT st.id FROM public.states_of_farmers st where st.description = '($1)'),
                                                            (SELECT f.id FROM farmers f where f.launcher_id = '($2)'))
                                                    ''', new_state, launcher_id)

        except asyncpg.Error as error:
            print("Failed to insert data into farmer_update_state", error)
        finally:
            if self.connection_payments:
                self.connection_payments.close()
                print("The PostgreSQL connection is closed")

    async def farmer_import(self,launcher_id):
        rows = []
        rows_p = []
        # recupero el farmers en la base local para validar que exista
        try:
            self.connection = sqlite3.connect(self.db_path)

            cursor = self.connection.cursor()
            cursor.execute( f"select * from farmer f where f.launcher_id =  '{launcher_id}'")
            rows= cursor.fetchall()
        except sqlite3.Error as error:
            print("Failed to select data into sqlite table", error)
        finally:
            if self.connection:
                self.connection.close()
                print("The SQLite connection is closed")

        if len(rows) == 0: 
            self.log.warning(f"{launcher_id} no existe en la base de datos de farmers ")
            return 0

        if not rows[0][10]: 
            self.log.warning(f"{launcher_id} no esta activo en esta pool")
            return 0

        farmer_chia = rows[0]

        wallet_hash = farmer_chia[9]

        # verifico que no este en la base de pagos
        query = f"SELECT f.id FROM public.farmers f JOIN public.coins c on ( f.coins_id = c.id )"\
                 f" where c.description = 'chia' and f.launcher_id ='{launcher_id}'"
        try:
            self.connection_payments = await self.connect_payments_system()
            rows_p = await self.connection_payments.fetch(query)
        except:
           print(f"Fallo al recuperar los datos del farmer en el sistema de pagos: {query}")
           return 0

        if len(rows_p) != 0: 
            self.log.info(f"{launcher_id} ya esta ingresado en el sistema de pagos")
            farmer_pay = rows_p[0]
            return farmer_pay[0]

        # importo el farmer y todos los datos
        self.log.info(f"Ingresando el farmer {launcher_id} al sistema de pagos")

        farmer_id_insert = 0

        try:
            async with self.connection_payments.transaction():

                self.log.info(f"Insertando estado de wallets")
                query = f"INSERT INTO public.current_states_of_wallets (datetime, states_of_wallet_id) "\
                    f" VALUES(CURRENT_TIMESTAMP, ( select id from states_of_wallet where description = 'activa') )"
                self.log.info(query)
                await self.connection_payments.execute( query )

                self.log.info(f"Recupero estado de wallets")
                state_wallet = await self.connection_payments.fetchval("SELECT MAX(id) FROM current_states_of_wallets")
        
                self.log.info(f"Insertando farmers")
                query = f"INSERT INTO public.farmers (coins_id, launcher_id)"\
                    f" VALUES((select c.id from coins c where c.description = 'CHIA'), '{launcher_id}'::bpchar)"
                self.log.info(query)
                await self.connection_payments.execute( query )

                self.log.info(f"Recupero farmers")
                farmer_id_insert = await self.connection_payments.fetchval("SELECT MAX(id) FROM farmers")

                self.log.info(f"Insertando wallet")
                query =  f"INSERT INTO public.wallets ( datetime_create, hash, coins_id)"\
                    f" VALUES( CURRENT_TIMESTAMP, '{wallet_hash}'::bpchar, (select c.id from coins c where c.description = 'CHIA'))"
                self.log.info(query)
                await self.connection_payments.execute( query )
        
                self.log.info(f"Recupero wallet")
                wallet_id_insert = await self.connection_payments.fetchval("SELECT MAX(id) FROM wallets")

                self.log.info(f"Insertando wallet de farmer")
                query =  f"INSERT INTO public.wallets_of_farmers (wallets_id, farmers_id, currents_state_of_walletes_id, datetime_create)"\
                    f" VALUES('{wallet_id_insert}', '{farmer_id_insert}', '{state_wallet}', CURRENT_DATE)"
                self.log.info(query)
                await self.connection_payments.execute( query )

                self.log.info(f"Insertando estado de farmer")
                query =  f"INSERT INTO public.current_states_of_farmers (datetime, states_of_farmers_id, farmers_id)"\
                    f" VALUES(CURRENT_TIMESTAMP, ( select id from states_of_wallet where description = 'activo'), '{farmer_id_insert}')"
                self.log.info(query)
                await self.connection_payments.execute( query )
        except:
            farmer_id_insert = 0
        return farmer_id_insert


    async def pay_to_farmer(self,launcher_id,generic_name,mount):
        mount_payout = 0
        farmer_id = 0
        wallet_id = 0
        payments_id = 0
        additions_sub_list: List[Dict] = []
        try:
            self.connection_payments = await self.connect_payments_system()
            async with self.connection_payments.transaction():
            # recupero el id del farmer
                query = f"select id from farmers where launcher_id ='{launcher_id}'"
                self.log.info(f"Recupero el id del farmer: {query}")
                farmer_id = await self.connection_payments.fetchval(query)
                if farmer_id == 0:
                    return mount_payout
            # con el ide obtengo la wallet del farmer
                query =  f"select w.id from farmers f"\
                            f" join wallets_of_farmers wof on ( wof.farmers_id = f.id)"\
                            f" join current_states_of_wallets csow on ( csow.id = wof.currents_state_of_walletes_id)"\
                            f" join states_of_wallet sow on (sow.id = csow.states_of_wallet_id)"\
                            f" join wallets w on ( wof.wallets_id = w.id)"\
                            f" join coins c on ( w.coins_id = c.id )"\
                            f" where sow.description = 'activa' and c.generic_mane = '{generic_name}' and f.id = '{farmer_id}' "
                self.log.info(f"Recupero el id de la wallet: {query}")
                wallet_id = await self.connection_payments.fetchval(query)
                if wallet_id == None:
                    return mount_payout
            # creo el pago y recupero el id
                self.log.info(f"Insertando pago")
                query =  f"INSERT INTO public.payments (description, datetime_create, types_of_payments_id, wallets_id, mount) "\
                    f" VALUES(''::bpchar, CURRENT_TIMESTAMP, 1, '{wallet_id}' , {mount} )"
                self.log.info(query)
                await self.connection_payments.execute( query )
        
                self.log.info(f"Recupero wallet")
                payments_id = await self.connection_payments.fetchval("SELECT MAX(id) FROM payments")
                if payments_id == None:
                    return mount_payout
            # creo el pago al farmer
                self.log.info(f"Insertando pago al farmer")
                query =  f"INSERT INTO public.payments_at_farmers (payments_id, farmers_id) "\
                    f" VALUES('{payments_id}', '{farmer_id}' )"
                self.log.info(query)
                await self.connection_payments.execute( query )
        
                query = f"select w.hash from wallets w where w.id  = '{wallet_id}'"
                self.log.info(f"Recupero wallet hash: {query}")
                wallet_hash = await self.connection_payments.fetchval(query)
                if wallet_hash == None:
                    return mount_payout
                self.log.info(f"agrego el pago a la lista {payments_id}, {wallet_hash}, {mount}")
                additions_sub_list.append({"payment_id" : payments_id, "puzzle_hash": wallet_hash, "amount": mount})# revisar que pueda entrar el id en pendingpayments
                await self.pending_payments.put(additions_sub_list.copy())
                self.log.info(f"el pago fue agregado con exito {additions_sub_list}")
        except:
            return 0
        return mount

    async def pay_closures_with_fe(self,closure_hash,fe):
        acumulated_amount = 0
        pool_amount = 0
        farmers_amount = 0
        total_points = 0
        pay_to_point = 0
        pay_amount = 0
        pay_to_farmers = 0
        try:
            self.connection_payments = await self.connect_payments_system()
            async with self.connection_payments.transaction():
        # recupero el monto acumulado del cierre
                query = f"select c.acumulated_amount from closures c where c.hash = '{closure_hash}'"
                self.log.info(f"Recupero el monto acumulado: {query}")
                acumulated_amount = await self.connection_payments.fetchval(query)
        # recupero los farmers del cierre junto con sus puntos y wallets
                query = f" select w.hash as wallet_hash, f.launcher_id as launcher_id , cp.points "\
                        f" from farmers f "\
                        f" join wallets_of_farmers wof on ( wof.farmers_id = f.id) "\
                        f" join current_states_of_wallets csow on ( csow.id = wof.currents_state_of_walletes_id) "\
                        f" join states_of_wallet sow on (sow.id = csow.states_of_wallet_id) "\
                        f" join wallets w on ( wof.wallets_id = w.id) "\
                        f" join coins c on ( w.coins_id = c.id ) "\
                        f" join closing_points cp on ( cp.farmers_id = f.id ) "\
                        f" join closures cr on ( cr.id = cp.closures_id) "\
                        f" where sow.description = 'activa' and c.generic_mane = 'xch' and cr.hash = '{closure_hash}'" 
                self.log.info(f"Recupero los datos de pago de los farmers: {query}")
                payment_details = await self.connection_payments.fetchall(query)

        # saco el porcentaje para la pool
                pool_amount = acumulated_amount * fe
                farmers_amount = acumulated_amount - pool_amount
        # calculo el pago por punto y realizo los pagos
                for payment_detail in payment_details:
                    total_points = total_points + payment_detail[2]
                pay_to_point = farmers_amount / total_points

                for payment_detail in payment_details:
                    pay_to_farmers = pay_to_point * payment_detail[2]
                    await self.pay_to_farmer(self,payment_detail[1],'XCH',pay_to_farmers)
                    pay_amount = pay_amount + pay_to_farmers
                    pay_to_farmers = 0
        except:
            return 0
        return pay_amount

    async def closures_import(self,closure_hash):
        self.connection = sqlite3.connect(self.db_path)
        self.connection_payments = await self.connect_payments_system()
        cursor = self.connection.cursor()
        farmers_count = 0

        # importar cierre
        ## recueperar cierre
        query = f" SELECT hash, \"timestamp\", acomulated_amount FROM closures where hash = '{closure_hash}'"
        self.log.info(f" recuperando cierre: {query}")
        cursor.execute(query)
        rows= cursor.fetchall()
        if len(rows) == 0: 
            self.log.warning(f"{closure_hash} no existe en la base de datos ")
            exit

        closure_timestamp = rows[0][1]
        closure_datetime = datetime.fromtimestamp(closure_timestamp)

        closure_amount = rows[0][2]

        self.log.info(f" la marca de tiempo es {closure_timestamp} ")
        self.log.info(f" el monto es {closure_amount} ")

        ## guardar cierre en el sistema de pagos
        try:
            async with self.connection_payments.transaction():
                self.log.info(f"Importando cierre")
                query = f" INSERT INTO public.closures (hash, datetime, acumulated_amount) "\
                    f" VALUES('{closure_hash}'::bpchar, '{closure_datetime}', {closure_amount})"
                self.log.info(query)
                await self.connection_payments.execute( query )
                self.log.info(f"Recupero cierre")
                closures_id_ps = await self.connection_payments.fetchval("SELECT MAX(id) FROM closures")
        except:
           print(f"Fallo al importar el cierre {closure_hash}")
           return 0

        # recupero todos los farmers del cierre
        cursor.execute( f" select fpoc.launcher_id , fpoc.points from closures c "\
                        f" join farmers_ponts_of_closure fpoc on ( c.hash = fpoc.closure_hash ) where c.hash = '{closure_hash}'" )
        farmers_closure_active = cursor.fetchall()
        self.connection.close()
        # importo cada farmer de la lista y le agrego sus puntos

        for farmer_temp in farmers_closure_active:
            farmer_temp_launcher_id = farmer_temp[0]
            farmer_temp_points = farmer_temp[1]
            self.log.info(f" launcher_id {farmer_temp_launcher_id} puntos {farmer_temp_points}")
            farmer_temp_id = await self.farmer_import(farmer_temp_launcher_id)
            if farmer_temp_id != 0:
                self.log.info(f"Importando cierre")
                query = f"INSERT INTO public.closing_points (points, farmers_id, closures_id) "\
                        f"  VALUES( {farmer_temp_points} , {farmer_temp_id} , {closures_id_ps})"
                self.log.info(query)
                await self.connection_payments.execute( query )
                farmers_count = farmers_count + 1
        return farmers_count

        i=0
        for row in rows:
            print(f" {i} {row}")
            i=i+1


    async def submit_payment_loop(self):
        while True:
            try:
                peak_height = self.blockchain_state["peak"].height
                await self.wallet_rpc_client.log_in_and_skip(fingerprint=self.wallet_fingerprint)
                if not self.blockchain_state["sync"]["synced"] or not self.wallet_synced:
                    self.log.warning("Waiting for wallet sync")
                    await asyncio.sleep(60)
                    continue

                payment_targets = await self.pending_payments.get()
                assert len(payment_targets) > 0

                self.log.info(f"Submitting a payment: {payment_targets}")

                # TODO(pool): make sure you have enough to pay the blockchain fee, this will be taken out of the pool
                # fee itself. Alternatively you can set it to 0 and wait longer
                # blockchain_fee = 0.00001 * (10 ** 12) * len(payment_targets)
                blockchain_fee: uint64 = uint64(0)
                try:
                    transaction: TransactionRecord = await self.wallet_rpc_client.send_transaction_multi(
                        self.wallet_id, payment_targets, fee=blockchain_fee
                    )
                except ValueError as e:
                    self.log.error(f"Error making payment: {e}")
                    await asyncio.sleep(10)
                    await self.pending_payments.put(payment_targets)
                    continue

                self.log.info(f"Transaction: {transaction}")

                while (
                    not transaction.confirmed
                    or not (peak_height - transaction.confirmed_at_height) > self.confirmation_security_threshold
                ):
                    transaction = await self.wallet_rpc_client.get_transaction(self.wallet_id, transaction.name)
                    peak_height = self.blockchain_state["peak"].height
                    self.log.info(
                        f"Waiting for transaction to obtain {self.confirmation_security_threshold}"\
                        f"confirmations, actualPeak: {peak_height} confirmed at height: {transaction.confirmed_at_height}"
                    )
                    if not transaction.confirmed:
                        self.log.info(f"Not confirmed. In mempool? {transaction.is_in_mempool()}")
                    else:
                        self.log.info(f"Confirmations: {peak_height - transaction.confirmed_at_height}")
                    await asyncio.sleep(10)

                # TODO(pool): persist in DB
                self.log.info(f"Successfully confirmed payments {payment_targets}")

            except asyncio.CancelledError:
                self.log.info("Cancelled submit_payment_loop, closing")
                return
            except Exception as e:
                # TODO(pool): retry transaction if failed
                self.log.error(f"Unexpected error in submit_payment_loop: {e}")
                await asyncio.sleep(60)

#___________________________________________________________________________________________________________________________________

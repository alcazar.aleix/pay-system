import asyncio
import logging
import os
import ssl
import time
import traceback
from typing import Dict, Callable, Optional

import aiohttp
import yaml
from blspy import AugSchemeMPL, G2Element
from aiohttp import web
from chia.protocols.pool_protocol import (
    PoolErrorCode,
    GetFarmerResponse,
    GetPoolInfoResponse,
    PostPartialRequest,
    PostFarmerRequest,
    PutFarmerRequest,
    validate_authentication_token,
    POOL_PROTOCOL_VERSION,
    AuthenticationPayload,
)
from chia.types.blockchain_format.sized_bytes import bytes32
from chia.util.byte_types import hexstr_to_bytes
from chia.util.hash import std_hash
from chia.consensus.default_constants import DEFAULT_CONSTANTS
from chia.consensus.constants import ConsensusConstants
from chia.util.json_util import obj_to_response
from chia.util.ints import uint8, uint64, uint32
from chia.util.default_root import DEFAULT_ROOT_PATH
from chia.util.config import load_config

from .record import FarmerRecord
from .payments_system import PaymetsSystem
from .store.abstract import AbstractPoolStore
from .util import error_response, RequestMetadata


def allow_cors(response: web.Response) -> web.Response:
    response.headers["Access-Control-Allow-Origin"] = "*"
    return response


def check_authentication_token(launcher_id: bytes32, token: uint64, timeout: uint8) -> Optional[web.Response]:
    if not validate_authentication_token(token, timeout):
        return error_response(
            PoolErrorCode.INVALID_AUTHENTICATION_TOKEN,
            f"authentication_token {token} invalid for farmer {launcher_id.hex()}.",
        )
    return None


def get_ssl_context(config):
    if config["server"]["server_use_ssl"] is False:
        return None
    ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ssl_context.load_cert_chain(config["server"]["server_ssl_crt"], config["server"]["server_ssl_key"])
    return ssl_context


class PoolServer:
    def __init__(self, config: Dict, constants: ConsensusConstants, pool_store: Optional[AbstractPoolStore] = None):

        # We load our configurations from here
        with open(os.getcwd() + "/config.yaml") as f:
            pool_config: Dict = yaml.safe_load(f)

        self.log = logging.getLogger(__name__)
        self.paymets_systems = PaymetsSystem(config, pool_config, constants, pool_store)

        self.pool_config = pool_config
        self.host = pool_config["server"]["server_host"]
        self.port = int(pool_config["server"]["server_port"])

    async def start(self):
        await self.paymets_systems.start()

    async def stop(self):
        await self.paymets_systems.stop()

    def wrap_http_handler(self, f) -> Callable:
        async def inner(request) -> aiohttp.web.Response:
            try:
                res_object = await f(request)
                if res_object is None:
                    res_object = {}
            except Exception as e:
                tb = traceback.format_exc()
                self.log.warning(f"Error while handling message: {tb}")
                if len(e.args) > 0:
                    res_error = error_response(PoolErrorCode.SERVER_EXCEPTION, f"{e.args[0]}")
                else:
                    res_error = error_response(PoolErrorCode.SERVER_EXCEPTION, f"{e}")
                return allow_cors(res_error)

            return allow_cors(res_object)

        return inner

    async def index(self, _) -> web.Response:
        return web.Response(text="Payouts Systems")

    async def closures_create(self, _) -> web.Response:
        hash_closure = await self.paymets_systems.create_closure()
        return obj_to_response({"closure-id":f"{hash_closure}"})

    async def closures_import(self, request_obj) -> web.Response:
        closure_hash = request_obj.match_info.get('closure_hash', "")
        farmers_count = await self.paymets_systems.closures_import(closure_hash)
        return obj_to_response({"farmers_count":f"{farmers_count}"})

    async def payuout_to_wallet(self, _) -> web.Response:
        res: GetPoolInfoResponse = GetPoolInfoResponse(
            self.paymets_systems.info_name,
            self.paymets_systems.info_logo_url,
            uint64(self.paymets_systems.min_difficulty),
            uint32(self.paymets_systems.relative_lock_height),
            POOL_PROTOCOL_VERSION,
            str(self.paymets_systems.pool_fee),
            self.paymets_systems.info_description,
            self.paymets_systems.default_target_puzzle_hash,
            self.paymets_systems.authentication_token_timeout,
        )
        return obj_to_response(res)


    async def farmer_update_state(self, request_obj) -> web.Response:
        new_state = request_obj.match_info.get('state', "")
        launcher_id = request_obj.match_info.get('launcher_id', "")
        if ( new_state != "" ) and ( launcher_id != "" ) :
            hash_closure = await self.paymets_systems.farmer_update_state(self,launcher_id,new_state)
        return obj_to_response()


    async def farmer_import(self, request_obj) -> web.Response:
        launcher_id = request_obj.match_info.get('launcher_id', "")
        if ( launcher_id != "" ) :
           farmer_id = await self.paymets_systems.farmer_import(launcher_id)
        return obj_to_response({'farmer_id':f"{farmer_id}"})

    
    async def pay_to_farmer(self, request_obj) -> web.Response:
        launcher_id = request_obj.match_info.get('launcher_id', "")
        generic_name = request_obj.match_info.get('generic_name', "")
        mount = request_obj.match_info.get('mount', "")
        mount_payout = await self.paymets_systems.pay_to_farmer(launcher_id,generic_name,mount)
        return obj_to_response({'mount_payout':f"{mount_payout}"})

    async def pay_closures_with_fe(self, request_obj) -> web.Response:
        closure_hash = request_obj.match_info.get('closure_hash', "")
        fe = request_obj.match_info.get('fe', "")
        total_mount_payout = await self.paymets_systems.pay_closures_with_fe(closure_hash,fe)
        return obj_to_response({'total_mount_payout':f"{total_mount_payout}"})


server: Optional[PoolServer] = None
runner: Optional[aiohttp.web.BaseRunner] = None


async def start_pool_server(pool_store: Optional[AbstractPoolStore] = None):
    global server
    global runner
    config = load_config(DEFAULT_ROOT_PATH, "config.yaml")
    overrides = config["network_overrides"]["constants"][config["selected_network"]]
    constants: ConsensusConstants = DEFAULT_CONSTANTS.replace_str_to_bytes(**overrides)
    server = PoolServer(config, constants, pool_store)
    await server.start()

    app = web.Application()
    app.add_routes(
        [
            web.get("/", server.wrap_http_handler(server.index)),
            web.post("/closures/create", server.wrap_http_handler(server.closures_create)),
            web.post("/closures/{closure_hash}/import", server.wrap_http_handler(server.closures_import)),
            #web.post("/closures/payuout-to-wallet", server.wrap_http_handler(server.payuout_to_wallet)),
            web.patch('/farmers/{launcher_id}/state/{state}', server.wrap_http_handler(server.farmer_update_state)),
            web.post('/farmers/{launcher_id}/import', server.wrap_http_handler(server.farmer_import)),
            web.post('/farmers/{launcher_id}/coins/{generic_name}/{mount}/payout', server.wrap_http_handler(server.pay_to_farmer)),
            web.post("/closures/{closure_hash}/{fe}/payout", server.wrap_http_handler(server.pay_closures_with_fe)),
        ]
    )
    runner = aiohttp.web.AppRunner(app, access_log=None)
    await runner.setup()
    ssl_context = get_ssl_context(server.pool_config)
    site = aiohttp.web.TCPSite(
        runner,
        host=server.host,
        port=server.port,
        ssl_context=ssl_context,
    )
    await site.start()

    while True:
        await asyncio.sleep(3600)


async def stop():
    await server.stop()
    await runner.cleanup()


def main():
    try:
        asyncio.run(start_pool_server())
    except KeyboardInterrupt:
        asyncio.run(stop())


if __name__ == "__main__":
    main()
